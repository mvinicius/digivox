#!/bin/sh

TIPO=$1
PROJETO=$2

if [[ -z "$TIPO" ]] || [[ -z "$PROJETO" ]]; then
	echo "uso: digivox [-p] [nome_do_projeto]"
	echo "-j : projeto java"
	echo "-w : projeto java web"
	exit
fi

if [ $TIPO == "-j" ]; then
	ARCHETYPE='maven-archetype-quickstart'
fi
if [ $TIPO == "-w" ]; then
	ARCHETYPE='maven-archetype-webapp'
fi

mvn archetype:generate -DgroupId=br.com.digivox.$PROJETO -DartifactId=$PROJETO -DarchetypeArtifactId=$ARCHETYPE -DinteractiveMode=false

#cd testando
#git init
#git add .
#git commit -m "init"
#mvn dependency:resolve
#mvn eclipse:eclipse